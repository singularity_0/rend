"""
    tests.unit.rend.test_jinja
    ~~~~~~~~~~~~~~

    Unit tests for the jinja renderer
"""
import os
import sys

import jinja2.exceptions
import pytest

import rend.exc


FDIR = os.path.join(sys.path[0].replace(os.getcwd(), ""), "files")


@pytest.mark.asyncio
async def test_jinja(mock_hub, hub):
    """
    test rend.jinja.render renders correctly
    """
    mock_hub.rend.jinja.render = hub.rend.jinja.render

    ret = await mock_hub.rend.jinja.render('{% set test = "itworked" %}{{ test }}')
    assert ret == "itworked"


@pytest.mark.asyncio
async def test_jinja_bytes(mock_hub, hub):
    """
    test rend.jinja.render renders correctly with bytes data
    """
    mock_hub.rend.jinja.render = hub.rend.jinja.render
    ret = await mock_hub.rend.jinja.render(b'{% set test = "itworked" %}{{ test }}')
    assert ret == "itworked"


@pytest.mark.asyncio
async def test_jinja_undefined(mock_hub, hub):
    """
    test rend.jinja.render when there is an undefined error
    """
    mock_hub.rend.jinja.render = hub.rend.jinja.render
    with pytest.raises(rend.exc.RenderException) as exc:
        await mock_hub.rend.jinja.render("{{ hello.test }}")
    assert exc.value.args[0] == "Jinja variable: 'hello' is undefined"


@pytest.mark.asyncio
async def test_jinja_syntax(mock_hub, hub):
    """
    test rend.jinja.render when there is a syntax error
    """
    mock_hub.rend.jinja.render = hub.rend.jinja.render
    with pytest.raises(rend.exc.RenderException) as exc:
        await mock_hub.rend.jinja.render("{% test % }")
    assert exc.value.args[0] == "Jinja syntax error: Encountered unknown tag 'test'."


@pytest.mark.asyncio
async def test_jinja_file_include(mock_hub, hub):
    """
    test rend.jinja.render renders correctly with an included file via jinja
    """
    mock_hub.rend.jinja.render = hub.rend.jinja.render
    # test include of raw yaml
    expected = "---\ntest:\n  foo: bar"
    fn = os.path.join(FDIR, "test.yml")
    ret = await mock_hub.rend.jinja.render('{% include "' + fn + '" %}')
    assert ret == expected


@pytest.mark.asyncio
async def test_jinja_file_import(mock_hub, hub):
    """
    test rend.jinja.render renders correctly with an imported variable via jinja
    """
    mock_hub.rend.jinja.render = hub.rend.jinja.render
    expected = "things"
    fn = os.path.join(FDIR, "import.sls")
    # test import of specific variable
    ret = await mock_hub.rend.jinja.render(
        '{% from "' + fn + '" import mytest %}{{ mytest }}'
    )
    assert ret == expected
    # test import of all variables/macros
    ret = await mock_hub.rend.jinja.render(
        '{% import "' + fn + '" as awesome %}{{ awesome.mytest }}'
    )
    assert ret == expected
    # test ability to break out of the path. this file exists, but fails due to presence of ".." in path
    fn = os.path.join(FDIR, f"..{os.sep}files{os.sep}import.sls")
    with pytest.raises(jinja2.exceptions.TemplateNotFound) as exc:
        await mock_hub.rend.jinja.render(
            '{% from "' + fn + '" import mytest %}{{ mytest }}'
        )
    assert exc.value.args[0] == fn


@pytest.mark.asyncio
async def test_jinja_base64_encode(mock_hub, hub):
    """
    test rend.jinja.render renders correctly for b64encode filter.
    """
    mock_hub.rend.jinja.render = hub.rend.jinja.render

    ret = await mock_hub.rend.jinja.render(
        '{% set test = "itworked" | b64encode %}{{ test }}'
    )
    assert ret == "aXR3b3JrZWQ="


@pytest.mark.asyncio
async def test_jinja_base64_encode_when_input_is_none(mock_hub, hub):
    """
    test rend.jinja.render should return empty string if the input is None.
    """
    mock_hub.rend.jinja.render = hub.rend.jinja.render

    ret = await mock_hub.rend.jinja.render(
        "{% set test = None | b64encode %}{{ test }}"
    )
    assert ret == ""


@pytest.mark.asyncio
async def test_jinja_base64_decode(mock_hub, hub):
    """
    test rend.jinja.render renders correctly for b64decode filter.
    """
    mock_hub.rend.jinja.render = hub.rend.jinja.render

    ret = await mock_hub.rend.jinja.render(
        '{% set test = "aXR3b3JrZWQ=" | b64decode %}{{ test }}'
    )
    assert ret == "itworked"


@pytest.mark.asyncio
async def test_jinja_base64_decode_when_input_is_none(mock_hub, hub):
    """
    test rend.jinja.render should return empty string if the input is None.
    """
    mock_hub.rend.jinja.render = hub.rend.jinja.render

    ret = await mock_hub.rend.jinja.render(
        "{% set test = None | b64decode %}{{ test }}"
    )
    assert ret == ""
